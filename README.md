# CSE 109 - Midterm Exam II

Spring 2020

## Ethics Contract

**FIRST**: Please read the following carefully:

-	I have not received, I have not given, nor will I give or receive, any assistance to another student taking this exam, including discussing the exam with students in another section of the course. Do not discuss the exam after you are finished until final grades are submitted.
-	I will not plagiarize someone else's work and turn it in as my own. If I use someone else's work in this exam, I will cite that work. Failure to cite work I used is plagiarism.
-	I understand that acts of academic dishonesty may be penalized to the full extent allowed by the Lehigh University Code of Conduct, including receiving a failing grade for the course. I recognize that I am responsible for understanding the provisions of the Lehigh University Code of Conduct as they relate to this academic exercise.

If you agree with the above, type your full name in the following space along with the date. Your exam **will not be graded** without this assent.

*** Writing your name and the date below binds you to the above agreement

Jingwen Wei 4/21/2020

*** Writing your name and the date above binds you to the above agreement

## Prelude

Now... on to the exam. There are two parts to this exam. In part 1 you're going to implement a binary tree data structure in C. If you need a refresher on binary trees [here's a good one](https://en.wikipedia.org/wiki/Binary_search_tree) that has implementations on the general algorithms. There's also literally a million videos on you tube about binary trees.

In the last two homeworks I gave you a skeleton project to get you started. For this exam, you're going to work from scratch. This repository is blank except for the readme. You are going to lay out the directory structure, create a Makefile, create a header file, create a library file, and then implement your tree, and then finally provide tests to verify the tree works. You can use any libraries and functions you like (except one implementing a binary tree of course!).

In part 2 I'll ask you to answer a couple open ended questions.

Some ground rules though: I'm going to divide the project into discrete "questions". E.g. question 1 will be to create blank files to flesh out your directory structure. After each question you need to commit your progress to gitlab. This is how I will know you are not just copying and pasting an entire solution from the internet. I will be able to see how much time you spend on each question based on your commit times. For each question, I want you to copy and paste the hash of the commit (you can copy the hash of the commit from the commit menu after you push to gitlab into this README. See this image: https://imgur.com/fSIRINb. The hash of the commit in the example image looks like this: 0258b14275288e117b704cce59d8cb7bb7b2c3c4). This means for each question you will have two commits: one to commit the work, and another to commit the hash into this readme. Each commit message will be the associate question number. For Question 1 the commit message will be "Question 1". And the subsequent hash will be "Question 1 hash".

If you complete a question and need to go back and modify code in a previous question that's okay. The hash should just represent your first commit of that code.

Each question will be worth the same number of points, for a total of 100 points for all questions. 

Also, **and this is very important** if you use any information from the internet or anywhere else, you have to **CITE IT** in your exam. This includes the wiki article I posted above. If you use that, cite it every time you use it. I don't care about the format, just make it clear.

## Part 1

### Question 1

Lay out your directory. Create a src folder, a include folder, and a blank Makefile. 

- Inside the `src` folder create a `bin` directory. 
- Inside the `src/bin` directory create `test.c`
- Inside the `src` directory create `lib.c`
- Inside the `include` directory, create `binarytree.h`

I forgot to hash the first question after I created the files
Hash: f0fbd292592d9515966b6a368d4050a798fd73a6

### Question 2

Implement the Makefile to build your project. It will be a little different than the HW so pay attention to these instructions.

- `make static` will generate a static library (libbinarytree.a) and put it in `build/lib`
- `make shared` will generate a shared library (libbinarytree.so) and put it in `build/lib`
- `make test` will generate a test executable from test.c and statically link to libbinarytree.a (it should make that as well if it doesn't exist). Put that in `build/bin`
- `make clean` will clean the project of all build artifacts (*.so *.a *.o test)

Hash: 3187ea1cfdc8e44630457d3a8a94f890bbf2652e

### Question 3

In `binarytree.h`, write a struct to represent the nodes of the binary tree. Each node should have at least:

- `void*` a pointer to an item held on the node
- `Node**` An array of pointers to other nodes

You can include any other fields you see fit to make this work.

Hash:b0a587108c959cee9c34b23a3c332f891229b4fe

### Question 4

In `binarytree.h`, write a struct to represent the binary tree. The binary tree should have at least:

- `Node*` a pointer to the root node
- `char` a character to hold the depth of the tree.

You can include any other fields you see fit to make this work.

Hash: 3c0b1754d50a2bed9047df13458b2f62f9c2728b

### Question 5

Stub out the relevant functions for your binary tree in `binarytree.h`

The functions you will implement are:

- `initBinaryTree()` takes a binary tree pointer and initialize the values on it.
- `insertItem()` takes a pointer to a binary tree and a pointer to an item and insert it into the tree. Returns `true` on success and `false` on failure (think about under what condition an insert would fail. It's probably not common). Updates the depth on the tree after each insert.
- `removeItem()` takes a pointer to a binary tree and a pointer to an item and remove that item from the tree if it exists in the tree. Returns a pointer to the item if the item was removed and `NULL` if the item was not removed (because it's not there). Updates the depth of the tree after each remove.
- `findItem()` takes a pointer to a binary tree and a pointer to an item and returns the depth of the item. It returns -1 if the item is not found in the list.
- `freeNodes()`takes a pointer to a binary tree and free all the nodes. It will not free the binary tree pointer.
- `printTree()` takes a pointer to a binary tree and print the entire tree. This can be in any format that helps you visualize the tree.

You can add any other function arguments you see fit to make your functions work.

Hash:38cb115c814b2f48ff4ef4813f4fad018d87c13a

### Question 6

Implement `initBinaryTree()` in `lib.c`

Hash:0f0949adfd53fc7052615e419cc50b66d9e7f4e3

### Question 7

Implement `insertItem()` in `lib.c`

Hash:ccfd46c6b20caf5afc9c3125863626f78c744900

### Question 8

Implement `printTree()` in `lib.c`

Hash: 6739c111b175f3e94c5144a3b857f165f14488f3

### Question 9

Implement `findItem()` in `lib.c`

Hash:fedce40c37e2297e4b9d69337f66bdc5bed6e486

### Question 10

Implement `removeItem()` in `lib.c`

Hash:4ab8860ec8bc5a1da19a0c517ef15b96f4f1ecd5

### Question 11

Implement `freeNodes()` in `lib.c`

Hash:14488bd9d9b9832379c23db7443bc7dd7f8bb9d0

### Question 12

Write at least 5 tests in `test.c` to verify your code works.

Hash:d4a254b32ae855f0ddb6b3af42a1b55d6a6d0261

### Bonus

Set up a test runner on gitlab so that your test runs automatically and passes with a green checkmark.

Hash:

## Part 2

Now answer the following questions. You can commit the answers here in this file. You'll do two commits again, one for the content, and one for the hash of the commit. These questions are very open ended. I want you to write as much as you can to convince me you know what you're talking about. Minimum 100 words for each question.

### Question 13

What is the difference between the stack and the heap? What variables go on the stack? What variables go on the heap?

A stack has a fixed size, small and contiguous whereas a heap can grow in size, dynamic capacity and is fragmented. 
A stack is automatically managed, whereas the heap need to be managed manually. 
A stack is easy to access and easy to write because we have index so we can use pointers to access elements directly. And the heap is slower to access.
A stack stores staticl, fixed size variables, and a heap styores variables of unknown or dynamic size. 

Hash:fbb9af983135c8e82271556f391ca76b93f791d4

### Question 14

What are the pros of hash tables? What are the cons? What are some applications where hash tables are useful, and why are they good for those applications?

For number of items that goes up to infinite, it is better to use a hash table because the complexity time would be close to O(1), whereas if we were to use linked list, the complexity time will be O(n).

The cons are: for small number of items, it is actually slower to store as a hash table. 
Imagine the graph where O(1) and O(n) are plotted. The beginning part (where the value of n is relatively small) is where O(n) is actually faster than O(1), so in that case, using a hash table would be too abundant.  

Some applications for the hash table is things like the table of content, when you want to search for a word in a online dictionary...
Hash table is good for these application because the input are usually very big (a textbook contains a lot of pages, the English languages has a lot of words), so by using the hash table, searching through these things would take less time.
Also, as for a table of content, it divides the textbook up into sections, so it would be easier to find a thing if you know what you are looking for exactly. Just like the hash table, if you know which array box you store the element in, it is easier to go through the linked list and then find the item you want. 


Hash: c775e0713ba673bef82d7ecfb54285d4b92dcc61

### Question 15

Create a segfault in your binary tree program and commit it to gitlab. What are the steps you would use in gdb to debug this segfault? How do you compile your program to use with gdb? Give the output of a stack trace at the point at which the segfault occurs. Fix the segfault again and commit the fix.

gdb main
run
backtrace

Hash of segfault:fbfe886381779427bae9f5b2001fb2eda6ce97c9
Hash of fix:
Hash of Question 15 answer:f7e1ecbfc90432aba5b3c3e809027cf61c59286f