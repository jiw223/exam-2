#include "binarytree.h"
#include <stdio.h>
#include <stdlib.h>
// #include <stdbool.h>
#include <iostream>


//  Initialize an empty Binary Tree
void initBinaryTree(BinaryTree* bt){
    bt -> root = NULL;
    bt -> depth = 0;
    bt -> root -> leftChild = NULL;
    bt -> root -> rightChild = NULL;
}

//  Insert new item into the tree
//Citataion for insertItem(): I wrote java code of b-tree last semester and I translate it into C
void insertItem(BinaryTree* bt, void* item){
    bool addReturn;
    Node* root = bt->root;
    root = insert(root, item, addReturn);
}


// Insert helper method
//Citataion for insertItem(): I wrote java code of b-tree last semester and I translate it into C
Node* insert(Node* localRoot, void* item, bool addReturn){
    if (localRoot == NULL){
        addReturn = true; 
        Node* node = (Node*)malloc(sizeof(Node));
        return node;
    }
    else if (item == localRoot -> item){
        addReturn = false;
        return localRoot;
    }
    else if (item < localRoot -> item){
        // Node* lc = localRoot -> leftChild;
        localRoot->leftChild = insert(localRoot -> leftChild, item, addReturn);
        return localRoot;
    }
    else {
        localRoot->rightChild = insert(localRoot -> rightChild, item, addReturn);
        return localRoot;
    }
}

//  Search for the item in the tree
void* findItem(BinaryTree* bt, void* item){
    return find(bt-> root, item);
}
// Search helper method
void* findItem(Node* localRoot, void* item){
    if (localRoot == NULL){
        return NULL;
    }
    if (localRoot->item == item){
        return localRoot -> item;
    }
    else if (localRoot->item < item){
        return find(localRoot->leftChild, item);
    }
    else {
        return find(localRoot->rightChild, item);
    }
}



//  Print the tree in any format
void printTree(BinaryTree* bt){
    Node* currNode = bt -> root;
    if (currNode == NULL){
        return NULL;
    }
    else {
        while(currNode -> leftChild != NULL){
            print(currNode->leftChild->item);
        }
        while(currNode -> rightChild != NULL){
            print(currNode->rightChild->item);
        }
    }
}



//  Remove an item from the tree
void removeItem(BinaryTree* bt, void* item){
    Node* root = bt->root;
    bool deleteReturn;
    root = removeH(root, item, deleteReturn);
    return deleteReturn;

}
// RemoveItem Helper method
Node* removeH(Node* localRoot, void* item, bool deleteReturn){
    if (localRoot == NULL){
        deleteReturn = NULL;
        return localRoot;
    }
    if (localRoot-> item < item){
        localRoot -> leftChild = removeH(localRoot->leftChild, item, deleteReturn);
        return localRoot;
    }
    else if (localRoot-> item > item){
        localRoot -> rightChild = removeH(localRoot->rightChild, item, deleteReturn);
        return localRoot;
    }
    else{ //find the target
        if (localRoot->leftChild == NULL){
            return localRoot->rightChild;
        }
        else if (localRoot->rightChild == NULL){
            return localRoot->leftChild;
        }
        else{
            return localRoot;
        }
    }
}




//  Free all the nodes of the tree
void freeNodes(BinaryTree* bt){
    Node* currNode = bt->root;
    bool freeReturn = false;
    while (!freeReturn){
        freeReturn = freeNode(currNode);
    }
}

// Free helper method
void freeNodes(Node* localRoot){
    if(localRoot -> leftChild != NULL){
        Node* tempLeft = localRoot->leftChild;
        freeNode(tempLeft);
    }
    else if (localRoot -> rightChild != NULL){
        Node* tempRight = localRoot -> rightChild;
        freeNode(tempRight);
    }
    else {
        return true;
    }
}





