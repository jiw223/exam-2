#include "binarytree.h"
#include "stdbool.h"

//citation: Prof Montella HW4/src/bin/test.c
int test(char* label, bool a, bool b){
    printf("%s", label);
    if (a==b){
        printf("Passed\n");
        return 1;
    }
    else {
        print("Failed");
        exit(1);
    }
}

//citation: Prof Montella HW4/src/bin/test.c
int main(){
    BinaryTree *bt;

    void* item;

    bt = malloc(sizeof(BinaryTree));

    initList(bt);

    int a = 0x12ab345;
    int b = 0xfeed425;
    int c = 0xabcd38;
    int d = 0x1235698;
    int e = 0x2c374;
    int f = 0xa0f8982;
    int g = 0x053eba;
    int h = 0x61c2f5e;
    int i = 0xe0295f2;
    int j = 0x5e700eaa;
    int k = 0xe75bf1;
    int l = 0x6baa9bb;
    int m = 0x8b1390a;
    int n = 0x59dd1;
    int o = 0xfc32;
    int p = 0xa9d89d8;
    int q = 0x5b2c9e;
    int r = 0x2431e3;
    int s = 0x1910a;
    int t = 0x72;

    insertItem(bt, &a);
    test("TEST 1", findItem(bt, &a), true); 
    insertItem(bt, &b);
    insertItem(bt, &t);
    test("TEST 2", removeItem(bt, &c), false); //can't delete c because c is not in the list
    test("TEST 3", findItem(bt, &t), true);
    insertItem(bt, &g);
    test("TEST 4", removeItem(bt, &i), true);
    test("TEST 5", findItem(bt, &k), false);
}