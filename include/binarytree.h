// #ifndef BinaryTree

#ifndef BINARYTREE
#define BINARYTREE

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "binarytree.h"


typedef struct {
    void* item;
    struct Node* leftChild;
    struct Node* rightChild;
}Node;


typedef struct {
    Node* root;
    char depth;
    Node* leftChild;
    Node* rightChild;
}BinaryTree;

//  Initialize an empty Binary Tree
void initBinaryTree(BinaryTree* bt);

//  Insert new item into the tree
void insertItem(BinaryTree* bt, void* item);

//  Remove an item from the tree
void removeItem(BinaryTree* bt, void* item);

//  Search for the item in the tree
void* findItem(BinaryTree* bt, void* item);

//  Free all the nodes of the tree
void freeNodes(BinaryTree* bt);

//  Print the tree in any format
void printTree(BinaryTree* bt);

#endif