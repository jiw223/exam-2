all: main
    mkdir -p build/bin
    mkdir -p build/lib
    mv *.a build/lib
    
main: main.o libbinarytree.a
	gcc main.o -o main -lbinarytree -L.
	
main.o: src/bin/main.c
	gcc src/bin/main.c -c -I include

libbinarytree.a: 
    gcc libbinarytree.a -a libbinarytree
    mv *.a build/lib

libbinarytree.so: 
    gcc libbinarytree.so -so libbinarytree
    mv *.so build/lib
    
test: test.o libbinarytree.a
    gcc -g test.c -o test -l libbinarytree -L
    mkdir -p build/bin
    mkdir -p build/lib
    mv *.a build/lib
    mv *.o build/lib
    
    
test.o: src/bin/test.c
    gcc src/bin/test.c -c -I include
    
    
libbinarytree.a: src/lib.c
    gcc src/lib.c -c -I include
    ar rs libbinarytree.a lib.o
        
        
clean: 
    rm -rf build
    rm -f test
    rm -f main
    